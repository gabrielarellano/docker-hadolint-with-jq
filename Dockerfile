FROM hadolint/hadolint:2.3.0-alpine

LABEL maintainer="gabrielarellano@gmail.com"

ENV JQ_VERSION=1.6-r1

# Install JQ
RUN apk add --no-cache jq=$JQ_VERSION


